<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class Validar extends Mailable
{
	use Queueable, SerializesModels;
	protected $datos;

	public function __construct($datos)
	{
		$this->datos = $datos;
	}

	public function build()
	{
		return $this->subject('Valida tu cuenta de correo')
			->view('correos.'.$this->datos['tipoUsuario'])
			->with(['enlace' => $this->datos['enlace']]);
	}
}
