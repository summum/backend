<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Crypt;

class AdministradorController extends Controller
{
	public function autenticar(Request $request){


		$estudiante = DB::table('administrador')
			->select('id')
			->addSelect('clave')
			->where('correo', $request->input('correo'))
			->first();

		if(!$estudiante)
			return ['result' => 'Este correo no existe'];

		try{
			
			if($request->input('clave') == Crypt::decrypt($estudiante->clave))
				return response()->json(['token' => $this->jwt($estudiante->id)], 200);
		}
	 	catch (DecryptException $e) {
			return ['result' => $e->getMessage()];
		}
	}

	protected function jwt($id) {
		$payload = [
			'iss' => "lumen-jwt", // Issuer of the token
			'sub' => $id, // Subject of the token
			'adm' => "si",
			'iat' => time(), // Time when JWT was issued.
			'exp' => time() + 60*60 // Expiration time
		];

		return JWT::encode($payload, env('JWT_SECRET'));
	}

}
