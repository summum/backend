<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use App\Mail\Validar;

class VoluntarioController extends Controller
{
	public function crear(Request $request){
		DB::beginTransaction();
		try {

			$voluntario = $request->all();
			$codigo = Crypt::encrypt(rand());
			$voluntario['codigo'] = $codigo;

			$id = DB::table('voluntario')->insertGetId($voluntario);

			$enlace = env('DOMINIO')."validation.php?id=$id&type=voluntario&codigo=$codigo";

			$datos = ['enlace' => $enlace, 'tipoUsuario' => 'voluntario'];

			Mail::to($request->input('correo'))->send(new Validar($datos));

			DB::commit();

			return ['result' => true];
		}
		catch(\Exception $e){
			DB::rollback();

			return ['result' => $e->getMessage()];
		}
	}

	public function ver($id, Request $request){
		if (strpos($request->header()['adm'][0], 'si') === false) {
			abort(403);
		}
		$results = DB::table('voluntario as v')
			->select('v.id')
			->join('estadoUsuario as e', 'e.id', '=', 'v.estado')
			->addSelect('v.nombre')
			->addSelect('v.apellido')
			->addSelect('v.pais')
			->addSelect('v.correo')
			->addSelect('v.telefono')
			->addSelect('v.sexo')
			->addSelect('v.fechaDeNacimiento')
			->addSelect('v.provincia')
			->addSelect('v.localidad')
			->addSelect('v.nivelDeEstudio')
			->addSelect('v.domicilio')
			->addSelect('v.fechaRegistro')
			->addSelect('e.nombre as estado')
			->where('v.id', $id)
			->where('v.validado', true)
			->first();

		return ['result' => $results];
	}

	public function actualizar(Request $request){
		if (strpos($request->header()['adm'][0], 'si') === false) {
			abort(403);
		}
		try{
			DB::table('voluntario')->where('id', $request->input('id'))->update($request->all());
			return ['result' => true];
		}
		catch(\Exception $e){
			return ['result' => $e->getMessage()];
		}
	}

	public function validar($id, $codigo){
		DB::beginTransaction();
		try{
			$result = DB::table('voluntario')
									->where('id', '=', $id)
									->where('codigo', '=', $codigo)
									->update(['validado' => true, 'codigo' => null]);

			if($result != 1)
				return ['result' => false];

			DB::commit();
			return ['result' => true];
		}
		catch(\Exception $e){
			DB::rollback();
			return ['result' => $e->getMessage()];
		}
	}

	public function listar(Request $request){
		if (strpos($request->header()['adm'][0], 'si') === false) {
			abort(403);
		}

		$nombre = $request->input('nombre');
		if($nombre != '')
			$results = DB::table('voluntario as v')
				->join('estadoUsuario as e', 'e.id', '=', 'v.estado')
				->select('v.id', 'v.nombre', 'v.apellido', 'v.correo', 'e.nombre as estado')
				->where(DB::raw('LOWER(v.nombre)'), 'like', DB::raw("LOWER('%$nombre%')"))
				->orWhere(DB::raw('LOWER(v.apellido)'), 'like', DB::raw("LOWER('%$nombre%')"))
				->orderBy('fechaRegistro', 'desc')
				->get();

		else
			$results = DB::table('voluntario as v')
				->join('estadoUsuario as e', 'e.id', '=', 'v.estado')
				->select('v.id', 'v.nombre', 'v.apellido', 'v.correo', 'e.nombre as estado')
				->where('validado', true)
				->orderBy('fechaRegistro', 'desc')
				->get();
		return ['result' => $results];
	}
}
