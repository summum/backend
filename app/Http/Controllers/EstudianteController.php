<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use App\Mail\Validar;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class EstudianteController extends Controller
{
	public function crear(Request $request){
		DB::beginTransaction();
		try {

			$estudiante = $request->all();

			$clave = Crypt::encrypt($request->input('clave'));
			$codigo = Crypt::encrypt(rand());

			$estudiante['clave'] = $clave;
			$estudiante['codigo'] = $codigo;

			$id = DB::table('estudiante')->insertGetId($estudiante);

			$enlace = env('DOMINIO')."validation.php?id=$id&type=estudiante&codigo=$codigo";

			$datos = ['enlace' => $enlace, 'tipoUsuario' => 'estudiante'];

			Mail::to($request->input('correo'))->send(new Validar($datos));

			DB::commit();

			return ['result' => true];
		}
		catch(Exception $e){
			DB::rollback();

			return ['result' => $e->getMessage()];
		}
	}

	public function ver($id, Request $request){
		$results = DB::table('estudiante as e')
			->join('estadoUsuario as estad', 'estad.id', '=', 'e.estado')
			->addSelect('e.nombre')
			->addSelect('e.apellido')
			->addSelect('e.pais')
			->addSelect('e.correo')
			->addSelect('e.telefono')
			->addSelect('e.sexo')
			->addSelect('e.fechaDeNacimiento')
			->addSelect('e.provincia')
			->addSelect('e.localidad')
			->addSelect('e.nivelDeEstudio')
			->addSelect('e.domicilio')
			->addSelect('e.fechaRegistro')
			->addSelect('estad.nombre as estado')
			->where('e.id', $id)
			->where('validado', true)
			->first();

		return ['result' => $results];
	}

	public function actualizar(Request $request){
		try{
			DB::table('estudiante')->where('id', $request->input('id'))->update($request->all());
			return ['result' => true];
		}
		catch(\Exception $e){
			return ['result' => $e->getMessage()];
		}
	}

	public function validar($id, $codigo){
		DB::beginTransaction();
		try{
			$result = DB::table('estudiante')
				->where('id', '=', $id)
				->where('codigo', '=', $codigo)
				->update(['validado' => true, 'codigo' => null]);

			if($result != 1)
				return ['result' => false];

			DB::commit();
			return ['result' => true];
		}
		catch(\Exception $e){
			DB::rollback();
			return ['result' => $e->getMessage()];
		}
	}

	public function listar(Request $request){
		if (strpos($request->header()['adm'][0], 'si') === false) {
			abort(403);
		}
		$nombre = $request->input('nombre');
		if($nombre != '')
			$results = DB::table('estudiante as e')
				->join('estadoUsuario as estad', 'estad.id', '=', 'e.estado')
				->select('e.id', 'e.nombre', 'e.apellido', 'e.correo', 'estad.nombre as estado')
				->where('validado', true)
				->where(DB::raw('LOWER(e.nombre)'), 'like', DB::raw("LOWER('%$nombre%')"))
				->orWhere(DB::raw('LOWER(e.apellido)'), 'like', DB::raw("LOWER('%$nombre%')"))
				->orderBy('fechaRegistro', 'desc')
				->get();

		else
			$results = DB::table('estudiante as e')
				->join('estadoUsuario as estad', 'estad.id', '=', 'e.estado')
				->select('e.id', 'e.nombre', 'e.apellido', 'e.correo', 'estad.nombre as estado')
				->where('validado', true)
				->orderBy('fechaRegistro', 'desc')
				->get();
		return ['result' => $results];
	}

	protected function jwt($id) {
		$payload = [
			'iss' => "lumen-jwt", // Issuer of the token
			'sub' => $id, // Subject of the token
			'adm' => "no",
			'iat' => time(), // Time when JWT was issued.
			'exp' => time() + 60*60 // Expiration time
		];

		return JWT::encode($payload, env('JWT_SECRET'));
	}

	public function autenticar(Request $request){
		$estudiante = DB::table('estudiante')
			->select('id')
			->addSelect('clave')
			->where('correo', $request->input('correo'))
			->first();

		if(!$estudiante)
			return ['result' => 'Este correo no existe'];

		try{
			if($request->input('clave') == Crypt::decrypt($estudiante->clave))
				return response()->json(['token' => $this->jwt($estudiante->id)], 200);
		}
	 	catch (DecryptException $e) {
			return ['result' => $e->getMessage()];
		}
	}
}
