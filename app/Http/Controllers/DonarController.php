<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use MP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DonarController extends Controller
{
	public function donar(Request $request){

		try{
			$mp = new MP (env('MP_APP_ID'), env('MP_APP_SECRET'));

			$data = $request->all();
			$data['currency_id'] = "ARS";
			$data['concept_type'] = "off_platform";

			$result = ($mp->post(
				array(
					"uri" => "/money_requests",
					"data" => $data
					)
				)
			);

			if($result['status'] == 201){
				$donacion['id'] = $result['response']['id'];
				$donacion['status'] = $result['response']['status'];
				$donacion['monto'] = $result['response']['amount'];
				$donacion['correo'] = $result['response']['payer_email'];

				DB::table('donacion')->insert($donacion);
				return ['result' => true];
			}

		}
		catch(\Exception $e){
			return ['result' => $e->getMessage()];
		}
	return ['result' => false];
	}
}
