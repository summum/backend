<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CursoController extends Controller
{
	public function crear(Request $request){
		if (strpos($request->header()['adm'][0], 'si') === false) {
			abort(403);
		}
		try {
			$curso = $request->all();
			DB::table('curso')->insert($curso);

			return ['result' => true];
		}
		catch(\Exception $e){
			return ['result' => $e->getMessage()];
		}
	}

	public function ver($id){
		$results = DB::table('curso as c')
			->join('estadoCurso as e', 'e.id', '=', 'c.estado')
			->select('c.id')
			->addSelect('titulo')
			->addSelect('descripcion')
			->addSelect('informacion')
			->addSelect('nombre as estado')
			->addSelect('inicio')
			->addSelect('fin')
			->addSelect('inicioInscripcion')
			->addSelect('finInscripcion')
			->addSelect('cupos')
			->where('c.id', $id)
			->first();

		return ['result' => $results];
	}

	public function actualizar(Request $request){
		if (strpos($request->header()['adm'][0], 'si') === false) {
			abort(403);
		}
		try{
			DB::table('curso')->where('id', $request->input('id'))->update($request->all());
			return ['result' => true];
		}
		catch(\Exception $e){
			return ['result' => $e->getMessage()];
		}
	}

	public function listar(Request $request){
		$titulo= $request->input('titulo');
		if($titulo != '')
			$results = DB::table('curso as c')
				->join('estadoCurso as e', 'e.id', '=', 'c.estado')
				->select('c.id')
				->addSelect('titulo')
				->addSelect('descripcion')
				->addSelect('informacion')
				->addSelect('nombre as estado')
				->addSelect('inicio')
				->addSelect('fin')
				->addSelect('inicioInscripcion')
				->addSelect('finInscripcion')
				->addSelect('cupos')
				->orderBy('titulo', 'desc')
				->where(DB::raw('LOWER(titulo)'), 'like', DB::raw("LOWER('%$titulo%')"))
				->orWhere(DB::raw('LOWER(descripcion)'), 'like', DB::raw("LOWER('%$titulo%')"))
				->orderBy('titulo', 'desc')
				->get();

		else
			$results = DB::table('curso as c')
			->join('estadoCurso as e', 'e.id', '=', 'c.estado')
			->select('c.id')
			->addSelect('titulo')
			->addSelect('descripcion')
			->addSelect('informacion')
			->addSelect('nombre as estado')
			->addSelect('inicio')
			->addSelect('fin')
			->addSelect('inicioInscripcion')
			->addSelect('finInscripcion')
			->addSelect('cupos')
			->orderBy('titulo', 'desc')
			->get();
		return ['result' => $results];
	}

	public function registrarEstudiante(Request $request){
		try{
			DB::table('estudiantecurso')->insert($request->all());
			return ['result' => true];
		}
		catch(\Exception $e){
			return ['result' => $e->getMessage()];
		}
	}

	public function inscritos($curso, Request $request){
		if (strpos($request->header()['adm'][0], 'si') === false) {
			abort(403);
		}
		try{
			$results = DB::table('estudiante as e')
				->join('estudianteCurso', 'id', '=', 'estudianteid')
				->join('estadoEstudiante as esta', 'esta.id', '=', 'e.estado')
				->select('e.id')
				->addSelect('e.nombre')
				->addSelect('apellido')
				->addSelect('pais')
				->addSelect('correo')
				->addSelect('telefono')
				->addSelect('sexo')
				->addSelect('fechaDeNacimiento')
				->addSelect('provincia')
				->addSelect('localidad')
				->addSelect('nivelDeEstudio')
				->addSelect('domicilio')
				->addSelect('esta.nombre as estado')
				->where('cursoid', $curso)
				->get();
			return ['result' => $results];
		}
		catch(\Exception $e){
			return ['result' => $e->getMessage()];
		}
	}

	public function certificado(Request $request)
	{
		if (strpos($request->header()['adm'][0], 'si') === false) {
			abort(403);
		}
		try{
			$path = $request->file('certificado')->store('certificados');

			DB::table('estudiantecurso')
				->where('estudianteid', $request->input('estudianteid'))
				->where('cursoid', $request->input('cursoid'))
				->update($request->all());

			return ['result' => true];
		}
		catch(\Exception $e){
			return ['result' => $e->getMessage()];
		}
	}
}
