<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtMiddleware
{
	public function handle($request, Closure $next, $guard = null)
	{

		if(!array_key_exists('token', $request->header()))
			return response()->json(['error' => 'Falta el token.'], 401);
		$token = $request->header()['token'][0];


	try {
		$credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
	} catch(ExpiredException $e) {
		return response()->json(['error' => 'El token expiró.'], 400);
	} catch(Exception $e) {
		return response()->json(['error' => 'Ocurrió un error decodificando el token.'], 400);
	}

	// Now let's put the user in the request class so that you can grab it from there
	$request->headers->set('id', $credentials->sub);
	$request->headers->set('adm', $credentials->adm);

	return $next($request);
	}
}
