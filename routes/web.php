<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $app->get('/', function () use ($app) {
//     return $app->version();
// });


//Donaciones
$app->post('donar', 'DonarController@donar');

// Voluntario

$app->get('voluntario/validar/{id}/{codigo}', 'VoluntarioController@validar');
$app->post('voluntario/crear', 'VoluntarioController@crear');

//Estudiante

$app->get('estudiante/validar/{id}/{codigo}', 'EstudianteController@validar');
$app->post('estudiante/crear', 'EstudianteController@crear');
$app->post('estudiante/login', 'EstudianteController@autenticar');

//Curso

$app->get('curso/{id}', 'CursoController@ver');
$app->post('curso/listar','CursoController@listar');


//AdministradorController
$app->post('administrador/login', 'AdministradorController@autenticar');

$app->group(['middleware' => 'jwt.auth'], function() use ($app) {
	$app->get('voluntario/{id}', 'VoluntarioController@ver');
	$app->get('estudiante/{id}', 'EstudianteController@ver');
	$app->post('voluntario/listar','VoluntarioController@listar');
	$app->post('voluntario/actualizar', 'VoluntarioController@actualizar');

	$app->post('estudiante/listar','EstudianteController@listar');
	$app->get('estudiante/{id}', 'EstudianteController@ver');
	$app->post('estudiante/actualizar', 'EstudianteController@actualizar');

	$app->post('curso/crear', 'CursoController@crear');
	$app->post('curso/actualizar', 'CursoController@actualizar');
	$app->post('curso/registrar', 'CursoController@registrarEstudiante');
	$app->get('curso/{curso}/inscritos', 'CursoController@inscritos');

});
