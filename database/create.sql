CREATE TABLE casadelpueblo.voluntario (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL,
	apellido VARCHAR(50) NOT NULL,
	pais VARCHAR(30) NOT NULL,
	correo VARCHAR(30) NOT NULL,
	telefono VARCHAR(20) NOT NULL,
	sexo VARCHAR(10) NOT NULL,
	fechaDeNacimiento DATE NOT NULL,
	provincia VARCHAR(20) NOT NULL,
	localidad VARCHAR(100) NOT NULL,
	nivelDeEstudio VARCHAR(20) NOT NULL,
	domicilio VARCHAR(100) NOT NULL,
	CONSTRAINT voluntario_PK PRIMARY KEY (id),
	CONSTRAINT voluntario_UN UNIQUE KEY (correo)
);
